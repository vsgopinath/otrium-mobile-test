const { config } = require('./shared.conf');

// ============
// Specs
// ============
config.specs = [
    './tests/specs/*.spec.js',
];

config.reporters = [['allure', {
    outputDir: 'allure-results',
    disableWebdriverStepsReporting: true,
    disableWebdriverScreenshotsReporting: true,
}]];

// ============
// Capabilities
// ============
// For all capabilities please check
// http://appium.io/docs/en/writing-running-appium/caps/#general-capabilities
config.capabilities = [
    {
        platformName: 'iOS',
        maxInstances: 1,
        'appium:deviceName': 'iPhone 12 Pro',
        'appium:platformVersion': '14.5',
        'appium:orientation': 'PORTRAIT',
        'appium:automationName': 'XCUITest',
        'appium:noReset': true,
        'appium:bundleId': 'Learning.eCoffee',
        'appium:newCommandTimeout': 480,
    },
];

exports.config = config;

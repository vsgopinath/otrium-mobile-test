const allure = require('allure-commandline');

exports.config = {

    onComplete: function () {
        const reportError = new Error('Could not generate Allure report');
        const generation = allure(['generate', 'allure-results', '--clean']);
        return new Promise((resolve, reject) => {
            const generationTimeout = setTimeout(
                () => reject(reportError),
                5000);

            generation.on('exit', function (exitCode) {
                clearTimeout(generationTimeout);

                if (exitCode !== 0) {
                    return reject(reportError);
                }

                console.log('Allure report successfully generated');
                resolve();
            });
        });
    },
    runner: 'local',
    framework: 'jasmine',
    jasmineNodeOpts: {
        defaultTimeoutInterval: 80000,
        helpers: [require.resolve('@babel/register')],
    },
    sync: true,
    logLevel: 'info',
    deprecationWarnings: true,
    waitforTimeout: 25000,
    connectionRetryTimeout: 25000,
    connectionRetryCount: 3,
    services: [
        [
            'appium',
            {
                logPath: './',
                args: {
                    relaxedSecurity: true,
                    port: 4726
                },
                command: 'appium',
            },
        ],
    ],
    port: 4723,
};

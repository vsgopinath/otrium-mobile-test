import AppScreen from './app.screen';

const SELECTORS = {
    ORDER_COFFEE_TEXT: '-ios class chain:**/XCUIElementTypeStaticText[`label CONTAINS "COFFEE"`]',
    PLACE_ORDER_BUTTON: '-ios class chain:**/XCUIElementTypeButton[`label CONTAINS "Place Order"`]'
};

class OrderScreen extends AppScreen {
    constructor () {
        super(SELECTORS.ORDER_COFFEE_TEXT);
    }

    get placeOrderButton () {
        return $(SELECTORS.PLACE_ORDER_BUTTON);
    }

    /**
     *
     * @param {String} coffeeName - Coffee Flavour Name
     */
    verifyCoffeeInOrder (coffeeName) {
        const selector = SELECTORS.ORDER_COFFEE_TEXT.replace('COFFEE', coffeeName);
        expect($(selector)).toBeDisplayed();
    }

    /**
     * Click Place Order
     */
    placeOrder () {
        this.placeOrderButton.click();
    }
}

export default new OrderScreen();

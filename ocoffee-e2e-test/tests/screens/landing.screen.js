import { MAX_SWIPE } from '../constants';
import AppScreen from './app.screen';

const SELECTORS = {
    CART_BUTTON: '~basket',
    LOGOUT_BUTTON: '~Log Out',
    DRINK_CATEGORY: '//XCUIElementTypeStaticText[contains(@value,"DRINK")]/following-sibling::XCUIElementTypeScrollView',
    COFFEE_BUTTON: '-ios class chain:**/XCUIElementTypeButton[`label CONTAINS "COFFEE"`]'
};

class LandingScreen extends AppScreen {
    constructor () {
        super(SELECTORS.CART_BUTTON);
    }

    get cartButton () {
        return $(SELECTORS.CART_BUTTON);
    }

    get logOutButton () {
        return $(SELECTORS.LOGOUT_BUTTON);
    }

    /**
     * Clicks on Logout
     */
    performLogOut () {
        this.logOutButton.click();
    }

    /**
     * Click on Basket
     */
    clickBasket () {
        this.cartButton.click();
    }

    /**
     *
     * @param {String} coffeeType - Name of the Coffee Category
     * @param {String} coffeeName - Name of the coffee flavour
     */
    selectCoffee (coffeeType, coffeeName) {
        const coffeeBtn = SELECTORS.COFFEE_BUTTON.replace('COFFEE', coffeeName);
        const drinkScrollSelector = SELECTORS.DRINK_CATEGORY.replace('DRINK', coffeeType);
        const scrollCoffeeView = $(drinkScrollSelector);
        if (!scrollCoffeeView.isDisplayed()) {
            this.swipeUp();
        }
        let i = 0;
        while (i < MAX_SWIPE) {
            if ($(coffeeBtn).getAttribute('visible') === 'true') {
                $(coffeeBtn).click();
                break;
            } else {
                this.swipeLeftInElement(scrollCoffeeView);
            }
            i = i + 1;
        }
    }
}

export default new LandingScreen();

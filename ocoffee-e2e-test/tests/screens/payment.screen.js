import AppScreen from './app.screen';

const SELECTORS = {
    PAY_MODE_BUTTON: '-ios class chain:**/XCUIElementTypeButton[`label CONTAINS "want  to pay?"`]',
    ADD_TIP_BUTTON: '-ios class chain:**/XCUIElementTypeButton[`label CONTAINS "TIP"`]',
    BACK_TO_ORDER_BUTTON: '-ios class chain:**/XCUIElementTypeButton[`label == "Order"`]',
    CONFIRM_ORDER: '-ios class chain:**/XCUIElementTypeButton[`name CONTAINS "Confirm Order"`]',
    ORDER_SUCCESS_TEXT: '-ios class chain:**/XCUIElementTypeStaticText[`label CONTAINS "Order confirmed"`]',
    OK_BUTTON: '-ios class chain:**/XCUIElementTypeButton[`label CONTAINS "OK"`]',
    BACK_TO_PAY_BUTTON: '-ios class chain:**/XCUIElementTypeButton[`label == "Payment"`]',

};

class OrderScreen extends AppScreen {
    constructor () {
        super(SELECTORS.ORDER_COFFEE_TEXT);
    }

    get payModeButton () {
        return $(SELECTORS.PAY_MODE_BUTTON);
    }

    get backToOrderButton () {
        return $(SELECTORS.BACK_TO_ORDER_BUTTON);
    }

    get backToPayButton () {
        return $(SELECTORS.BACK_TO_PAY_BUTTON);
    }

    get orderSuccessText () {
        return $(SELECTORS.ORDER_SUCCESS_TEXT);
    }

    get confirmOrderButton () {
        return $(SELECTORS.CONFIRM_ORDER);
    }

    get okButton () {
        return $(SELECTORS.OK_BUTTON);
    }

    /**
     *
     * @param {String} payName - Payment Mode Name
     */
    changePayMode (payName) {
        this.payModeButton.click();
        $(SELECTORS.ADD_TIP_BUTTON.replace('TIP', payName)).click();
    }

    /**
     * Adds Tip Amount
     * @param {String} tipAmount - Tip Amount to be selected
     */
    addTip (tipAmount) {
        $(SELECTORS.ADD_TIP_BUTTON.replace('TIP', tipAmount)).click();
    }

    /**
     * Click on Confirm Order
     */
    confirmOrder () {
        this.confirmOrderButton.click();
    }

    /**
     * Verify Order Confirmation Popup
     */
    verifyConfirmSuccess () {
        expect(this.orderSuccessText).toBeDisplayed();
        expect(this.okButton).toBeDisplayed();
        this.okButton.click();
    }
}

export default new OrderScreen();

import AppScreen from './app.screen';

const SELECTORS = {
    BACK_BUTTON: '-ios class chain:**/XCUIElementTypeButton[`label CONTAINS "iCoffee"`]',
    ADD_BASKET_BUTTON: '-ios class chain:**/XCUIElementTypeButton[`label CONTAINS "Add to basket"`]',
    SUCCESS_TEXT: '-ios class chain:**/XCUIElementTypeButton[`label CONTAINS "Added to Basket"`]',
    OK_BUTTON: '-ios class chain:**/XCUIElementTypeButton[`label CONTAINS "OK"`]'
};

class SelectScreen extends AppScreen {
    constructor () {
        super(SELECTORS.BACK_BUTTON);
    }

    get backButton () {
        return $(SELECTORS.BACK_BUTTON);
    }

    get addBasketButton () {
        return $(SELECTORS.ADD_BASKET_BUTTON);
    }

    get successText () {
        return $(SELECTORS.SUCCESS_TEXT);
    }

    get okButton () {
        return $(SELECTORS.OK_BUTTON);
    }

    /**
     * Adds the coffee to the basket or cart
     * @param {String} coffeeName - Name of the Coffee Flavour
     */
    addToBasket (coffeeName) {
        const selector = '-ios class chain:**/XCUIElementTypeStaticText[`label CONTAINS "COFFEE"`]'.replace('COFFEE', coffeeName);
        expect($(selector)).toBeDisplayed();
        this.addBasketButton.click();
    }

    /**
     *  Verify the success message on Add to Basket
     */
    verifySuccess () {
        expect(this.successText).toBeDisplayed();
    }

    /**
     * Click OK on Confirmation Popup
     */
    clickOk () {
        this.okButton.click();
    }

    /**
     * Click Back to Landing Button
     */
    backToLanding () {
        this.backButton.click();
    }
}

export default new SelectScreen();

import AppScreen from './app.screen';

const SELECTORS = {
    EMAIL_TEXTFIELD: '-ios class chain:**/XCUIElementTypeTextField[`value CONTAINS "email"`]',
    PASSWORD_TEXTFIELD: '-ios class chain:**/XCUIElementTypeSecureTextField',
    SIGNIN_BUTTON: '-ios class chain:**/XCUIElementTypeButton[`label CONTAINS "Sign In"`]',
    SIGNUP_BUTTON: '-ios class chain:**/XCUIElementTypeButton[`label CONTAINS "Sign Up"`]'
};

class LoginScreen extends AppScreen {
    constructor () {
        super(SELECTORS.SIGNIN_BUTTON);
    }

    get userName () {
        return $(SELECTORS.EMAIL_TEXTFIELD);
    }

    get password () {
        return $(SELECTORS.PASSWORD_TEXTFIELD);
    }

    get signInButton () {
        return $(SELECTORS.SIGNIN_BUTTON);
    }

    /**
     *
     * @param {String} emailText - Email
     * @param {String} passwordText - Password
     */
    performSignIn (emailText, passwordText) {
        this.userName.setValue(emailText);
        this.password.setValue(passwordText);
        this.signInButton.click();
    }
}

export default new LoginScreen();

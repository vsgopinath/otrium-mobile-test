import { DEFAULT_TIMEOUT, TOUCH_TIMEOUT } from '../constants';

export default class AppScreen {
    constructor (selector) {
        this.selector = selector;
    }

    /**
     * Wait for screen to be visible
     *
     * @param {boolean} isShown
     * @return {boolean}
     */
    waitForIsShown (isShown = true) {
        return $(this.selector).waitForDisplayed({
            timeout: DEFAULT_TIMEOUT,
            reverse: !isShown,
        });
    }

    /**
     * Wait for screen to be visible
     *
     * @param {String} selector
     * @param {Integer} timeout
     * @return {boolean}
     */
    waitForElement (selector, timeout) {
        return $(selector).waitForDisplayed({
            timeout: timeout,
            reverse: !true,
        });
    }

    /**
     * Generic Swipe up on device
     */
    swipeUp () {
        const deviceSize = driver.getWindowSize();
        console.log('Sizes : ' + Math.round(deviceSize.width * 0.5) + '' + Math.round(deviceSize.height * 0.5));
        this.swipe({ x: Math.round(deviceSize.width * 0.5), y: Math.round(deviceSize.height * 0.8) }, { x: Math.round(deviceSize.width * 0.5), y: Math.round(deviceSize.height * 0.2) });
    }

    /**
     * Swipes within the element size
     *
     * @param {WebdriverIO.Element} element
     */
    swipeLeftInElement (element) {
        const height = element.getSize('height');
        const width = element.getSize('width');
        const x = element.getLocation('x');
        const y = element.getLocation('y');
        this.swipe({ x: Math.round(x + width * 0.95), y: Math.round(y + height * 0.5) }, { x: Math.round(x + width * 0.03), y: Math.round(y + height * 0.5) });
    }

    /**
     * Perform Swipe event
     *
     * @param {Integer} from
     * @param {Integer} to
     */
    swipe (from, to) {
        driver.touchPerform([{
            action: 'press',
            options: from,
        }, {
            action: 'wait',
            options: { ms: TOUCH_TIMEOUT },
        }, {
            action: 'moveTo',
            options: to,
        }, {
            action: 'release',
        }]);
        driver.pause(TOUCH_TIMEOUT);
    }

    resetApp () {
        driver.reset();
    }
}

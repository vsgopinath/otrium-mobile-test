import coldCoffee from '../data/cold.coffee';
import filterCoffee from '../data/filter.coffee';
import hotCoffee from '../data/hot.coffee';
import login from '../data/login';
import landingScreen from '../screens/landing.screen';
import loginScreen from '../screens/login.screen';
import orderScreen from '../screens/order.screen';
import paymentScreen from '../screens/payment.screen';
import selectScreen from '../screens/select.screen';

describe('oCoffee Mobile Test - Order different Coffee Drinks', () => {
    beforeEach(() => {
        landingScreen.waitForIsShown(true);
        landingScreen.performLogOut();
        landingScreen.clickBasket();
        loginScreen.performSignIn(login.username, login.password);
        landingScreen.waitForIsShown(true);
    });

    it('should be able to save hot coffee drink', () => {
        landingScreen.selectCoffee(hotCoffee.category, hotCoffee.coffeeName);
        selectScreen.waitForIsShown(true);
        selectScreen.addToBasket(hotCoffee.coffeeName);
        selectScreen.clickOk();
        selectScreen.backToLanding();
        landingScreen.clickBasket();
        orderScreen.verifyCoffeeInOrder(hotCoffee.coffeeName);
        orderScreen.placeOrder();
        paymentScreen.changePayMode('Card');
        paymentScreen.addTip(hotCoffee.tip);
        paymentScreen.confirmOrder();
        paymentScreen.verifyConfirmSuccess();
    });

    it('should be able to buy cold coffee drink', () => {
        landingScreen.selectCoffee(coldCoffee.category, coldCoffee.coffeeName);
        selectScreen.waitForIsShown(true);
        selectScreen.addToBasket(coldCoffee.coffeeName);
        selectScreen.clickOk();
        selectScreen.backToLanding();
        landingScreen.clickBasket();
        orderScreen.verifyCoffeeInOrder(coldCoffee.coffeeName);
        orderScreen.placeOrder();
        paymentScreen.addTip(coldCoffee.tip);
        paymentScreen.confirmOrder();
        paymentScreen.verifyConfirmSuccess();
    });

    it('should be able to save and buy filter coffee drink', () => {
        landingScreen.selectCoffee(filterCoffee.category, filterCoffee.coffeeName);
        selectScreen.waitForIsShown(true);
        selectScreen.addToBasket(filterCoffee.coffeeName);
        selectScreen.clickOk();
        selectScreen.backToLanding();
        landingScreen.clickBasket();
        orderScreen.verifyCoffeeInOrder(filterCoffee.coffeeName);
        orderScreen.placeOrder();
        paymentScreen.changePayMode('Card');
        paymentScreen.addTip(filterCoffee.tip);
        paymentScreen.confirmOrder();
        paymentScreen.verifyConfirmSuccess();
    });

    afterEach(() => {
        loginScreen.resetApp();
    });
});

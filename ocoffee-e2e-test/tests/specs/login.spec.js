import login from '../data/login';
import landingScreen from '../screens/landing.screen';
import loginScreen from '../screens/login.screen';

describe('oCoffee Mobile Test - Login', () => {
    beforeEach(() => {
        landingScreen.waitForIsShown(true);
        landingScreen.performLogOut();
    });

    it('should be able login successfully', () => {
        landingScreen.clickBasket();
        loginScreen.performSignIn(login.username, login.password);
        expect(landingScreen.waitForIsShown(true)).toBe(true);
    });

    it('should not able to login', () => {
        landingScreen.clickBasket();
        loginScreen.performSignIn(login.username, login.wrongPassword);
        expect(loginScreen.waitForIsShown(true)).toBe(true);
    });

    afterEach(() => {
        loginScreen.resetApp();
    });
});

export const DEFAULT_TIMEOUT = 20000;
export const TOUCH_TIMEOUT = 2000;
export const MAX_SWIPE = 8;
export const VISIBLE_TIMEOUT = 2000;

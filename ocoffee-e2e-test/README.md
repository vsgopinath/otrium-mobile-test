# otrium-exercise
   Mobile Automation Test using WebDirverIO,Jasmine & Allure

## Packages Used

* [Allure](https://www.npmjs.com/package/allure-commandline)
* [WebDriverIO](https://webdriver.io/)
* [WDIO Jasmine](https://www.npmjs.com/package/@wdio/jasmine-framework)

## Tools Used

* VS Code
* yarn or npm
* [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)
* [Appium](https://appium.io/)
* [node](https://nodejs.org/en/) - v14

## Test Execution

* Install the dependencies:
  > `npm install`

* Run All the tests:
  > `npm run ios.app`

## Test Result
   * Allure Report are generated in `${PROJECT-ROOT}/ocoffee-e2e-test/allure-reports` (Need Live Server to open report)

## Project Details
    * UI Objects and interactions are added inside `screens` folder
    * Tests are in the `specs` folder
    * Capabilities and other test configuration are in the `config` folder

## Note
   * `runTests.sh` is a sample shell script to run both UI and E2E Tests.
   * UI Tests Results are found in `${PROJECT-ROOT}/Reports/`


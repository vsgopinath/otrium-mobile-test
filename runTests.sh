if ! command -v xcpretty &> /dev/null
then
    echo "xcpretty could not be found"
    echo "Installing xcpretty"
    gem install xcpretty
    exit
else
    echo "xcpretty already exists!!"
fi
echo "Disable Hardware keyboard before running the tests"
# killall Simulator
defaults write com.apple.iphonesimulator ConnectHardwareKeyboard -bool false
echo "Removing old reports and results"
rm -rf TestResults
rm -rf TestResults.xcresult
rm -rf Reports
echo "Installing Dependencies"
pod install
echo "Running oCoffee UI Tests and also installs the app"
set -o pipefail && xcodebuild test -workspace eCoffee.xcworkspace -scheme eCoffeeUITests -destination 'platform=iOS Simulator,name=iPhone 12 Pro,OS=14.5' -resultBundlePath TestResults | xcpretty --report html --output Reports/report.html
echo "Installing Dependencies"
cd ocoffee-e2e-test
npm install
echo "Running E2E tests"
npm run ios.app


//
//  Landing.swift
//  eCoffeeUITests
//
//  Created by Gopinath Sreenivasan Viswanathan on 2021-07-03.
//

import XCTest

protocol LandingSteps {
    
}

extension XCUITestBase {
    func launchApp() {
        XCTContext.runActivity(named: "Check the app launch") { _ in
            XCUIApplication().launch()
            XCTAssertTrue(LandingScreen.title.element.exists)
        }
    }
    func gotoLogin() {
        XCTContext.runActivity(named: "Tap on Cart Button") { _ in
            LandingScreen.logoutButton.element.tap()
            LandingScreen.cartButton.element.tap()
        }
    }
    func checkLogin() {
        XCTContext.runActivity(named: "Check Login Screen") { _ in
            XCTAssertTrue(LoginScreen.userName.element.exists)     }
    }
    
}

//
//  LoginSteps.swift
//  eCoffeeUITests
//
//  Created by Gopinath Sreenivasan Viswanathan on 2021-07-03.
//

import XCTest

protocol LoginSteps {
    
}

extension XCUITestBase {
    func performLogin(userName:String, password:String) {
        XCTContext.runActivity(named: "Perform Login") { _ in
            XCTAssertTrue(LoginScreen.userName.element.exists)
            LoginScreen.userName.element.tap()
            LoginScreen.userName.element.typeText(userName)
            LoginScreen.password.element.tap()
            LoginScreen.password.element.typeText(password)
            LoginScreen.signInButton.element.tap()
        }
    }
}

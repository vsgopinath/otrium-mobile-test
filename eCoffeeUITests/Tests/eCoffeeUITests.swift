//
//  eCoffeeUITests.swift
//  eCoffeeUITests
//
//  Created by Gopinath Sreenivasan Viswanathan on 2021-07-03.
//

import XCTest

class eCoffeeUITests:  XCUITestBase, LandingSteps,LoginSteps  {
    
    func testLogin() {
        launchApp()
        gotoLogin()
        checkLogin()
        performLogin(userName: "gupta.akki23@gmail.com", password: "123456")
        XCTAssertTrue(LandingScreen.cartButton.element.exists)
    }
    
    func testInValidLogin() {
        launchApp()
        gotoLogin()
        checkLogin()
        performLogin(userName: "gupta.akki23@gmail.com", password: "123457")
        XCTAssertTrue(LoginScreen.signInButton.element.exists)
    }
    
}

//
//  LandingScreen.swift
//  eCoffeeUITests
//
//  Created by Gopinath Sreenivasan Viswanathan on 2021-07-03.
//

import XCTest

enum LandingScreen: String {
    
    case title = "iCoffee"
    case cartButton = "basket"
    case logoutButton = "Log Out"
    case addToBasketButton = "Add to basket"
    
    var element: XCUIElement {
        switch self {
        case .title:
            return XCUIApplication().staticTexts[self.rawValue]
        case .cartButton:
            return XCUIApplication().buttons[self.rawValue]
        case .logoutButton:
            return XCUIApplication().buttons[self.rawValue]
        case .addToBasketButton:
            return XCUIApplication().buttons[self.rawValue]

        }
        
    }
}

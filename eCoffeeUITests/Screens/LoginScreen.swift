//
//  LoginScreen.swift
//  eCoffeeUITests
//
//  Created by Gopinath Sreenivasan Viswanathan on 2021-07-03.
//

import XCTest

enum LoginScreen: String {
    
    case userName = "Enter your email"
    case password = "password"
    case signInButton = "Sign In"
    
    var element: XCUIElement {
        switch self {
        case .userName:
            return XCUIApplication().textFields[self.rawValue]
        case .signInButton:
            return XCUIApplication().buttons[self.rawValue]
        case .password:
            return XCUIApplication().secureTextFields["Enter your password"]
        }
    }
}

